/* 
 * File:   main.c
 * Author: Mathias
 *
 * Created on 21 septembre 2018, 11:49
 */

#pragma config OSC = INTIO67    // osc interne
#pragma config WDT = OFF	
#pragma config MCLRE = ON
#pragma config PBADEN = OFF
#pragma config LVP = OFF

#include <p18f2520.h>
#include <stdio.h>
#include <stdlib.h>

#define     True        1
#define     False       0


/*==============================================
=            VARIABLES GLOBALES                =
===============================================*/
 union {
    struct {
    unsigned Vbatt :1;   
    unsigned timer50ms:1;
    unsigned timer1s:1;  
    unsigned timer3s:1;  
    unsigned :1;
    unsigned :1;

    unsigned :1;
    unsigned :1;
    unsigned :1;
    unsigned :1;
    unsigned :1;
    unsigned :1;
    unsigned :1;
    };
    unsigned int MesDrapeaux;
}Flag;

void interruptions();

/*==============================================
=                 INTERRUPTIONS                =
===============================================*/
#pragma code InterruptVectorHigh = 0x08	// interruption haut niveau des PIC18 vectoris�e � cette adresse
void InterruptVectorHigh (void){
    _asm GOTO interruptions _endasm    //pose du vecteur pointant sur notre routine d'interruption
}
#pragma code    // retourne � une zone de codage normale

#pragma interrupt interruptions
void interruptions(){
    static unsigned char count_tmr_50ms = 0;
    static unsigned char count_tmr_1s = 0;
    static unsigned char count_tmr_3s = 0;

  // Timer2 Interrupt- Freq = 100.00 Hz - Period = 0.010000 seconds
    if (PIR1bits.TMR2IF == 1){
        count_tmr_50ms++;           // increment des counter
        count_tmr_1s++;

        PIR1bits.TMR2IF = 0;        // acquitement interuption 

        if(count_tmr_50ms == 5){      // flag logiciel a� 50ms
            Flag.timer50ms = True;
            count_tmr_50ms = 0;
        }
        if(count_tmr_1s == 100){    // flag logiciel a� 1s
            Flag.Vbatt = True;
            Flag.timer1s = True;
            count_tmr_1s = 0;

            if(count_tmr_3s >= 3){
                Flag.timer3s = True;
                count_tmr_3s = 0;
            }else{
                count_tmr_3s++;
            }
        }
    }
}

/*==============================================
=            FCT INITIALISATIONS               =
===============================================*/
void init_GPIO(){
    // Configuration E/S
    TRISAbits.TRISA0 = 1; // in
    TRISAbits.TRISA1 = 1; // in
    TRISAbits.TRISA2 = 1; // in
    TRISAbits.TRISA6 = 0; // out
    TRISAbits.TRISA7 = 0; // out
    TRISBbits.TRISB1 = 0; // out
    TRISBbits.TRISB5 = 0; // out
    TRISBbits.TRISB6 = 0; // out
}


void init_ADC(){

    ADCON1bits.PCFG=0b1100;  //configure les pin AN2, AN1,  AN0 en analogique

    ADCON1bits.VCFG1=0;  //tension de référence basse du micro à VSS (0V)
    ADCON1bits.VCFG0=0;  //tension de référence haute du micro à VDD (5V)

    ADCON2bits.ADCS=0b100;   //pour avoir un TAD de 1us, ce qui est suppérieur au 0,7us minimum
    ADCON2bits.ACQT=0b011;   //temps d'aquisition de 6us, ce qui est plus que le temps d'aquisition calculé de 4,2us

    // résolution de 20mv
    ADCON2bits.ADFM=0;   //justification à gauche
    ADCON0bits.ADON=1;   //activation de l'ADC
    ADCON0bits.GO_NOT_DONE=1; //lance la conversion

}

void start(){ //
    while(Flag.timer3s == False); // wait 3seconds
    Flag.timer3s = False;
}

void init_OSC(){
    OSCCON = 0x72;  // Oscillateur interne Fosc = 8MHz
}

void init_TIMER2(){
    //Timer2 Registers Prescaler= 16 - TMR2 PostScaler = 1 - PR2 = 125 - Freq = 1000.00 Hz - Period = 0.001000 seconds
    T2CON |= 72;            // bits 6-3 Post scaler 1:1 thru 1:16
    T2CONbits.TMR2ON = 1;   // bit 2 turn timer2 on;
    T2CONbits.T2CKPS1 = 1;  // bits 1-0  Prescaler Rate Select bits
    T2CONbits.T2CKPS0 = 0;
    PR2 = 125;              // PR2 (Timer2 Match value)
}

void init_INTERRUPT(){
    INTCON = 0;                  // clear the interrpt control register
    INTCONbits.TMR0IE = 0;       // bit5 TMR0 Overflow Interrupt Enable bit...0 = Disables the TMR0 interrupt
    PIR1bits.TMR2IF = 0;         // clear timer1 interupt flag TMR1IF
    PIE1bits.TMR2IE = 1;         // enable Timer2 interrupts
    INTCONbits.TMR0IF = 0;       // bit2 clear timer 0 interrupt flag
    INTCONbits.GIE = 1;          // bit7 global interrupt enable
    INTCONbits.PEIE = 1;         // bit6 Peripheral Interrupt Enable bit...1 = Enables all unmasked peripheral interrupts
}

void main(){
    /*==============================================
    =               INITIALISATIONS                =
    ===============================================*/

    init_GPIO();
    init_OSC();
    init_TIMER2();
    init_INTERRUPT();
    start();

    /*==============================================
    =               BOUCLE INFINIE                 =
    ===============================================*/
    while(1){

        if(Flag.timer50ms == True){    // interuption timer 50ms
            Flag.timer50ms = False;
        }

        if(Flag.Vbatt == True){    // interuption 1sec
            Flag.Vbatt = False;

        }

        if(Flag.timer3s == True){    // interuption 1sec
            Flag.timer3s = False;
            PORTBbits.RB5 = ~LATBbits.LATB5;// LED TEST
        }



    }

}

